package org.adoptopenjdk.betterrev.utils;

import java.io.*;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO Check if there is a better way to run processes in Java 8
public final class Processes {

    private final static Logger LOGGER = LoggerFactory.getLogger(Processes.class);
    
    private static final String SHELL = "/bin/sh";

    private Processes() {
        //  Hide Utility Class Constructor - Utility classes should not have a public or default constructor.
    }

    private static class StreamLogger extends Thread {

        private final String prefix;
        private final InputStream input;

        private StreamLogger(String prefix, InputStream input) {
            this.prefix = prefix;
            this.input = input;
        }

        @Override
        public void run() {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
                String line;
                while((line = reader.readLine()) != null) {
                    LOGGER.info(prefix + " > " + line);
                }
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    public static int runProcess(String workingDirectory, String... command) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(command);
        builder.directory(new File(workingDirectory));

        Process process = builder.start();

        StreamLogger outputLogger = new StreamLogger("out", process.getInputStream());
        outputLogger.start();

        StreamLogger errorLogger = new StreamLogger("err", process.getErrorStream());
        errorLogger.start();

        try {
            int exitCode = process.waitFor();

            if (exitCode != 0) {
                LOGGER.error("Unknown exit code when running " + Arrays.toString(command) + " value is " + exitCode);
            }

            return exitCode;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static int runThroughShell(String workingDirectory, String... args) throws IOException {
        String[] command = new String[args.length + 1];
        command[0] = SHELL;
        System.arraycopy(args, 0, command, 1, args.length);
        return runProcess(workingDirectory, command);
    }

}
