package org.adoptopenjdk.betterrev.update.pullrequest;

import java.util.List;

import org.adoptopenjdk.betterrev.models.Contribution;

/**
 * @author Edward Yue Shung Wong
 */
public final class PullRequestsImportedEvent {
    private List<Contribution> contributions;

    public PullRequestsImportedEvent(List<Contribution> contributions) {
        this.contributions = contributions;
    }

    public List<Contribution> getContributions() {
        return contributions;
    }
}
