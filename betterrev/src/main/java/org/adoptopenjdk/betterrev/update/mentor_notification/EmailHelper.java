package org.adoptopenjdk.betterrev.update.mentor_notification;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.adoptopenjdk.betterrev.models.ContributionEvent;
import org.adoptopenjdk.betterrev.models.Mentor;

/**
 * TODO: Issue #65 refactor this class into a full actor.
 *
 * @author arkangelofkaos
 */
public class EmailHelper {

    private static final String FROM_EMAIL = " betterrev@googlegroups.com";
    private static final String HTML_MIME_TYPE = "text/html";

    private final Session mailSession;

    public EmailHelper() {
        this.mailSession = makeSession();
    }

    private static Session makeSession() {
        // TODO Get from pure properties
        /*Configuration conf = Play.application().configuration();
        Properties props = new Properties();
        loadPropertyFromConfiguration(conf, props, "mail.smtp.host");
        loadPropertyFromConfiguration(conf, props, "mail.smtp.port");
        */
        Properties properties = new Properties();
        return Session.getDefaultInstance(properties);
    }

    /*
    private static void loadPropertyFromConfiguration(Configuration from, Properties to, String propertyName) {
        String value = from.getString(propertyName);
        to.setProperty(propertyName, value);
    }
    */

    public void sendEmailMessage(ContributionEvent request) throws MessagingException {
        MimeMessage emailMessage = createEmail(request);
        renderEmailContent(request, emailMessage);
        Transport.send(emailMessage);
    }

    private MimeMessage createEmail(ContributionEvent request) throws MessagingException {
        MimeMessage emailMessage = new MimeMessage(mailSession);
        emailMessage.setFrom(new InternetAddress(FROM_EMAIL));
        setRecipients(request.getContribution().getMentors(), emailMessage);
        return emailMessage;
    }

    private static void setRecipients(Set<Mentor> mentors, MimeMessage emailMessage) throws MessagingException {
        List<InternetAddress> addresses = new ArrayList<>(mentors.size());
        for (Mentor mentor : mentors) {
            addresses.add(new InternetAddress(mentor.getEmail()));
        }
        emailMessage.setRecipients(Message.RecipientType.TO, addresses.toArray(new Address[mentors.size()]));
    }

    private static void renderEmailContent(ContributionEvent request, MimeMessage emailMessage) throws MessagingException {
        emailMessage.setSubject("RFR: " + request.getContribution().getName());
        // TODO
        // Html body = ContributionCreated.render(request.contribution);
        // emailMessage.setContent(body.toString(), HTML_MIME_TYPE);
    }

}
