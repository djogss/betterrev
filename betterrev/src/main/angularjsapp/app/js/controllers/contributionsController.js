betterrevApp.controller('contributionsController', ['$scope', 'contributionsService',
    function ($scope, contributionsService) {

        $scope.contributions = contributionsService.query();
    }]
);
