betterrevApp.controller('tagsController', function ($scope, $modal, $log, tagsService) {
    $scope.tags = tagsService.query();

    $scope.openModal = function (tag) {

        var modalInstance = $modal.open({
            templateUrl: 'tagModal',
            controller: 'tagModalController',
            size: 'sm',
            resolve: {
                tag: function () {
                    return tag;
                }
            }
        });

        modalInstance.result.then(function () {
            $scope.tags = tagsService.query();
        }, function () {

        });
    };

    $scope.delete = function (tag) {
        tag.$delete(function () {
            $scope.tags = tagsService.query();
        });
    }
});

betterrevApp.controller('tagModalController', function ($scope, $log, tagsService, $modalInstance, tag) {

    if (tag) {
        $scope.tag = {
            id: tag.id,
            name: tag.name,
            description: tag.description
        };
    } else {
        $scope.tag = {};
    }

    var action = (tag === undefined) ? 'save' : 'update';

    $scope.modalAction = (tag === undefined) ? 'Create' : 'Update';

    $scope.isValid = true;

    $scope.validateAndSave = function () {

        if (!$scope.tag.name) {
            $scope.isValid = false;
            return;
        }

        tagsService[action]($scope.tag).$promise.then(function () {
            $modalInstance.close();
        }, function () {
            $log.error('An error occurred for action: ' + action);
            $modalInstance.dismiss('error');
        });
    };

    $scope.close = function () {
        $modalInstance.dismiss('close');
    };
});