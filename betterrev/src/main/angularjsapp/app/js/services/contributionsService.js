/**
 * A service factory which creates an object with methods for fetching 
 * contributions from the REST service.
 */
betterrevApp.factory('contributionsService', ['$resource', 'environment', function ($resource, environment) {
     return $resource(environment.baseURL + 'betterrev/contributions/:contributionId', {contributionId: '@id'});
}]);

